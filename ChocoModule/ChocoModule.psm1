
#region test
function Test-Configuration {
    [CmdletBinding()]
    param (
        [object]$p_from_file
    )
    # upgradable and pinned are incompatible:
    $p_upgradable_pinned = $p_from_file | Where-Object {$_.upgradable -eq "True" } |  Where-Object {$_.pinned -eq "True" }
    if ( $null -ne $p_upgradable_pinned) {
        # Raise exception
        Write-Host "Incompatible upgradable & pinned configuration, exiting: "$p_upgradable_pinned.name
        exit 1
    }

    # Installed version is different that configured version
    $p_installed_hash = Get-InstalledPackagesHashList
    foreach ($package in $p_installed_hash) {        
        foreach($file_package in $p_from_file){
            
            if ( ($file_package.name -eq $package.name) -and 
                 (-not [string]::IsNullOrWhiteSpace($file_package.version)) -and
                 ($file_package.version -ne $package.version) ) {
                # Raise Warning
                Write-Host "Package $($file_package.name) is installed at version $($package.version) " `
                            "but configured in file as version $($file_package.version)"
            }
        }
    }

    
}

#endregion

#region functions
function Get-PackagesToInstall {
    [CmdletBinding()]
    param(
        [object]$p_from_file
    )

    $p_installed_hash_list = Get-InstalledPackagesHashList
    #Write-Host "From file $($config_file) hash: "$p_from_file   
    
    #$already_installed = $p_from_file | Where-Object {$_.name -in $p_installed_hash_list.foreach({ "$($_.name)"})}
    #Write-Host "Already installed: " $already_installed
    $p_from_file | Where-Object {$_.name -notin $p_installed_hash_list.foreach({ "$($_.name)"})}    
}

function Get-PackagesToUpgrade {
    [CmdletBinding()]
    param (
        [object]$p_from_file
    )
    $p_upgradable_from_file = $p_from_file | Where-Object {$_.upgradable -eq "True" }
    
    #Write-Host "From file $($config_file) upgradable hash: " $p_upgradable_from_file    
    $p_outdated = Get-OutdatedPackages
    $p_upgradable_from_file | Where-Object {$_.name -in $p_outdated}   

}

function  Get-PackagesToPin {
    [CmdletBinding()]
    param (
        [object]$p_from_file
    )
    $p_to_pin = $p_from_file | Where-Object {$_.pinned -eq "True" }
    $p_pinned = Get-PinnedPackagesHashList
    $p_to_pin | Where-Object {$_.name -notin $p_pinned.foreach({"$($_.name)"})}

}

function  Get-PackagesToUnPin {
    [CmdletBinding()]
    param (
        [object]$p_from_file
    )
    $p_to_unpin = $p_from_file | Where-Object {$_.pinned -ne "True" }
    $p_pinned = Get-PinnedPackagesHashList
    $p_to_unpin | Where-Object {$_.name -in $p_pinned.foreach({"$($_.name)"})}
}

function Get-ChocoPackagesFromJSON {    
    [CmdletBinding()]
    param (        
        [array]$content
    )

    $j_obj = $content | ConvertFrom-Json
    $file_packages = $j_obj.packages
    #Write-Host "Local file packages: "$file_packages
    
    foreach ($remote in $j_obj.remote_files) {
        #Write-Host "Entontrada URI: "  $remote.uri
        $response = Invoke-WebRequest -URI $remote.uri        
        # Recursive
        $remote_file_packages += Get-ChocoPackagesFromJSON($response)        
    }
    if ($remote_file_packages.count -gt 0 ) {
        $new_remote_packages = $remote_file_packages | Where-Object {$_.name -notin $file_packages.name}
        $file_packages += $new_remote_packages   
    }
    $file_packages
}

function Get-InstalledPackages {
    [CmdletBinding()]
    $chocolist = choco "list" "-l" | Out-String    
    
    $chocolist = $chocolist -replace "^.*\n",""
    $chocolist = $chocolist -replace "\n.*$",""        
    $chocolist.Split([Environment]::NewLine) | Where-Object {$_ -notmatch "^$"}      
}

function Get-PinnedPackages {
    [CmdletBinding()]
    $chocolist = choco "pin" "list" | Out-String    
    $chocolist = $chocolist -replace "^.*\n",""
    $chocolist.Split([Environment]::NewLine) | Where-Object {$_ -notmatch "^$"}      

}

function Get-InstalledPackagesHashList{
    [CmdletBinding()]
    $p_list = Get-InstalledPackages
    [hashtable[]]$packageHash_list = @()

    foreach ($item in $p_list) {
        $pair = $item.Split(" ")
        $hs = @{ 
            "name" = $pair[0] 
            "version" = $pair[1] 
        }        
        $packageHash_list += $hs        
    }    
    $packageHash_list
}

function Get-PinnedPackagesHashList{
    [CmdletBinding()]
    $p_list = Get-PinnedPackages
    [hashtable[]]$packageHash_list = @()

    foreach ($item in $p_list) {
        $pair = $item.Split("|")
        $hs = @{ 
            "name" = $pair[0] 
            "version" = $pair[1] 
        }        
        $packageHash_list += $hs        
    }    
    $packageHash_list
}
function Get-OutdatedPackages {
    [CmdletBinding()]
    $chocolist = choco "outdated" | Out-String   
    $chocolist = $chocolist -replace "^.*\n.*\n.*\n.*\n",""
    #Write-Host "Clean choco Outdated: " $chocolist
    # Filtering lines to eliminate lines empty       
    $list1 = $chocolist.Split([Environment]::NewLine) | Where-Object {$_.Trim -notmatch "^$"} 

    $cleanList = @()
    foreach ($item in $list1) {
        if ($item -match "\|"){
            $cleanList += $item.Split("|")[0]
        }        
    }    
    $cleanList    
}

function Install-ChocoPackage{
    [CmdletBinding()]
    param (
        [object]$package
    )
    
    $cparams = @( "install", $package.name, "-y", "--limitoutput" )
    if (-not [string]::IsNullOrWhiteSpace($package.version)){
        $cparams += "--version"
        $cparams += $package.version
    }

    if (-not [string]::IsNullOrWhiteSpace($package.installArguments)){
        $cparams += $package.installArguments.Split(" ")
    }
    
    choco $cparams
}

function Update-ChocoPackage{
    [CmdletBinding()]
    param (
        [object]$package
    )    
    $cparams = @( "upgrade", $package.name, "-y", "--limitoutput" )
    if (-not [string]::IsNullOrWhiteSpace($package.upgradeArguments)){
        $cparams += $package.upgradeArguments.Split(" ")
    }    
    choco $cparams
}

function Set-PinChocoPackage{
    [CmdletBinding()]
    param (
        [object]$package
    )    
    $cparams = @( "pin", "-y", "--limitoutput", "add", "--name", $package.name  )
    if (-not [string]::IsNullOrWhiteSpace($package.version)){
        $cparams += "--version"
        $cparams += $package.version
    }
    
    choco $cparams
}

function Remove-PinChocoPackage{
    [CmdletBinding()]
    param (
        [object]$package
    )    
    $cparams = @( "pin", "-y", "--limitoutput", "remove", "--name", $package.name  )
    if (-not [string]::IsNullOrWhiteSpace($package.version)){
        $cparams += "--version"
        $cparams += $package.version
    }
    
    choco $cparams
}

#endregion

Export-ModuleMember -Function * -Alias *