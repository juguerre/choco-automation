function Save-Shortcut{
    [CmdletBinding()]
    param (        
        [string]$shorcut_path
    )

    $WshShell = New-Object -comObject WScript.Shell
    $shortcut = $WshShell.CreateShortcut($shorcut_path)
    $shortcut.TargetPath = "powershell.exe" 
    $shortcut.Arguments = "-ExecutionPolicy unrestricted -File $($PSSCriptRoot)\main.ps1" 
    $shortcut.IconLocation = "$($PSSCriptRoot)\Project-choco.ico"
    $shortcut.WorkingDirectory = "$($PSSCriptRoot)"
    $shortcut.Description= "Start Chocolatey software Sync"
    
    $shortcut.Save()
    # Run as admin flag
    $bytes = [System.IO.File]::ReadAllBytes($shorcut_path)
    $bytes[0x15] = $bytes[0x15] -bor 0x20 #set byte 21 (0x15) bit 6 (0x20) ON
    [System.IO.File]::WriteAllBytes($shorcut_path, $bytes)

}

Save-Shortcut("$Home\Desktop\ChocoSync.lnk")