# Requires -RunAsAdministrator
Import-Module $PSScriptRoot\ChocoModule\ChocoModule.psm1


function Test-Administrator {  
    $user = [Security.Principal.WindowsIdentity]::GetCurrent();
    (New-Object Security.Principal.WindowsPrincipal $user).IsInRole([Security.Principal.WindowsBuiltinRole]::Administrator)  
}
function Test-InternetConn {
    "200" -eq (Invoke-WebRequest "google.com" | Select-Object statuscode).statuscode       
}

function Install-Choco{
    Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
}

function Save-Shortcut{
    [CmdletBinding()]
    param (        
        [string]$shorcut_path
    )

    $WshShell = New-Object -comObject WScript.Shell
    $shortcut = $WshShell.CreateShortcut($shorcut_path)
    $shortcut.TargetPath = "powershell.exe" 
    $shortcut.Arguments = "-ExecutionPolicy unrestricted -File $($PSSCriptRoot)\main.ps1" 
    $shortcut.IconLocation = "$($PSSCriptRoot)\Project-choco.ico"
    $shortcut.WorkingDirectory = "$($PSSCriptRoot)"
    $shortcut.Description= "Start Chocolatey software Sync"
    
    $shortcut.Save()
    # Run as admin flag
    $bytes = [System.IO.File]::ReadAllBytes($shorcut_path)
    $bytes[0x15] = $bytes[0x15] -bor 0x20 #set byte 21 (0x15) bit 6 (0x20) ON
    [System.IO.File]::WriteAllBytes($shorcut_path, $bytes)

}

function Test-ChocoInstall {

    #$regpath = @(
    #        'HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall\*'
    #        'HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\*'
    #    )
    #Get-ItemProperty $regpath | .{process{if($_.DisplayName -and $_.UninstallString) { $_ } }} | 
    #    Select DisplayName, Publisher, InstallDate, DisplayVersion, UninstallString |        
    #    where {$_.DisplayName -ilike "*vagrant*"}
    #    Sort DisplayName
   
    try {        
        $res = choco "--version" 2>&1 | Out-String
    }      
    catch [System.Management.Automation.CommandNotFoundException] {
        "Choco command not found ... Installing:"
        Install-Choco
    }
    finally{
        $choco_ver = choco "--version"
        Write-Host "Choco version installed: "$choco_ver
    }

}
# MAIN  

#Save-Shortcut("$Home\Desktop\ChocoSync.lnk")
$createShortcutPS1 = Join-Path $PSScriptRoot "create-shorcut.ps1"
& $createShortcutPS1

if ( -not (Test-Administrator) ){
    write-host "PowerShell admin right are required in order to operate with Chocolatey. Exiting"
    write-host "Try with your new desktop shortcut Choco-Sync"
    exit 1
}
if ( -not (Test-InternetConn) ){
    write-host "Internet connection needed to choco operation. Exiting ..."
    exit 1
}

# Read from json file
$filepath = "$($PSScriptRoot)\conf-choco-personal.json"
$p_from_file = Get-ChocoPackagesFromJSON((Get-Content -Path $filepath))

# Run some installation test
Test-Configuration($p_from_file)      

# Install packages not in intalled list
$chocoToInstall = Get-PackagesToInstall($p_from_file)
Write-Host "Ready to Install: "  $chocoToInstall.name.Count
foreach ($package in $chocoToInstall) {
    Install-ChocoPackage($package)
}

# Upgrade packages outdated that are taged as "upgradable"
$chocoToUpgrade = Get-PackagesToUpgrade($p_from_file)
Write-Host "Ready to upgrade: " $chocoToUpgrade.name.Count
foreach ($package in $chocoToUpgrade) {
    Update-ChocoPackage($package)
}

# Pin packages that are taged as "pinned"
$chocoToPin = Get-PackagesToPin($p_from_file)
Write-Host "Ready to pin: " $chocoToPin.name.Count
foreach ($package in $chocoToPin) {
    Set-PinChocoPackage($package)
}

# UnPin packages that are not tagged as "pinned"
$chocoToUnPin = Get-PackagesToUnPin($p_from_file)
Write-Host "Ready to Un-pin: " $chocoToUnPin.Count
foreach ($package in $chocoToUnPin) {
    Remove-PinChocoPackage($package)
}

Pause

#(New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1')

#https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/invoke-expression?view=powershell-7

#Set-ExecutionPolicy Bypass -Scope Process -Force 
#[System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072
#iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
