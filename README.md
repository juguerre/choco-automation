# README

Installs a list of prefered win software via chocolatey. A good way to start from scrach with a bunch of sofware and keep it updated.

## Direct remote install

Copy and execute on powershell console with admin rights:

```powershell

Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://gitlab.com/juguerre/choco-automation/-/raw/master/remote-install.ps1'))

```

A directory with the actual configuration will be found here:

```powershell
c:\users\<user>\choco-automation\choco-automation-master
```

And a shorcut with `Choco-Sync` in the user desktop.

## Installations steps

- **Download** project as Zip

    ![Download Zip](download-zip.png)

- **Extract** in local filesystem
- **Edit** `conf-choco-personal` and add or delete software packages. [package list](https://chocolatey.org/packages).
- **Execute** `main.ps1` in powershell with _admin_ rights.

> Note 1: As a shorcut you can set properties in order to execute always **with admin rights**. Remember that you need to use `powershell.exe ... -File <path to repository>\main.ps1` in order to get the possibility to set execute as admin.
> Read: https://www.groovypost.com/howto/make-windows-10-apps-always-run-with-administrator-privileges/

> Note 2: PowerShell shorcut command in order to execute the script without restrictions should be:
>`powershell.exe -ExecutionPolicy unrestricted -File <path to repository>\main.ps1`

> Note 3: It's possible to put a `main.ps1` shortcut in your desktop and use image file `Project-choco.ico` file as the shorcut icon.
> Read: https://www.isumsoft.com/windows-10/how-to-change-icon-for-desktop-shortcut.html

## Features

- checks admins rights (`choco` needs admin rights for install commands )
- checks internet connection
- checks if chocolatey is installed and installs if it's not
- Upgrade chocolatey
- Installs prefered software if not already installed
- Upgrades upgradable software list if possible
- Pin / Unpin software as configured in configuration file. see: [choco pin comand](https://chocolatey.org/docs/CommandsPin#:~:text=Pin%20Command%20(choco%20pin),a%20package%20to%20suppress%20upgrades.&text=alternative%20is%20choco%20upgrade%20%2D%2Dexcept%3D%22pkg1%2Cpk2%22%20.)  
- Remote file extension: if a remote-file configuration is found the process will follow these links and will add its content to the final configuration.

## Configuration file for Software to Install

The list of packages you want to install is defined in the repository file `conf-choco-personal.json`, change, add, delete whatever package name you want.
> See choco [package list](https://chocolatey.org/packages)
>Example:

```json
[
    {
        "packages": [
            {
                "name": "7zip",
                "version": "18.06",
                "installArguments": "--force",
                "upgradable": "True",
                "pinned": "False",
                "upgradeArguments": ""
            },
            {
                "name": "chocolatey",
                "installArguments": "",
                "upgradable": "True"
            },
            {
                "name": "curl",
                "installArguments": "--force --no-progress",
                "version": "7.50.1",
                "upgradable": "True",
                "upgradeArguments": ""
            }

        ],
        "remote_files": [
            {
                "uri": "https://gitlab.com/juguerre/choco-automation/-/raw/feature/remote-json-templates/conf-choco-general.json",
                "allow-upgrade": "True"
            },
            {
                "uri": "https://gitlab.com/juguerre/choco-automation/-/raw/feature/remote-json-templates/conf-choco-group1.json",
                "allow-upgrade": "True"
            }
        ]
    }
]
```
