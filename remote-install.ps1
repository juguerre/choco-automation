
#Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://gitlab.com/juguerre/choco-automation/-/raw/master/remote-install.ps1'))


function Get-Downloader {
    param (
        [string]$url
    )

    $downloader = new-object System.Net.WebClient

    $defaultCreds = [System.Net.CredentialCache]::DefaultCredentials
    if ($defaultCreds -ne $null) {
        $downloader.Credentials = $defaultCreds
    }

    $ignoreProxy = $env:chocolateyIgnoreProxy
    if ($ignoreProxy -ne $null -and $ignoreProxy -eq 'true') {
        Write-Debug "Explicitly bypassing proxy due to user environment variable"
        $downloader.Proxy = [System.Net.GlobalProxySelection]::GetEmptyWebProxy()
    }
    else {
        # check if a proxy is required
        $explicitProxy = $env:chocolateyProxyLocation
        $explicitProxyUser = $env:chocolateyProxyUser
        $explicitProxyPassword = $env:chocolateyProxyPassword
        if ($explicitProxy -ne $null -and $explicitProxy -ne '') {
            # explicit proxy
            $proxy = New-Object System.Net.WebProxy($explicitProxy, $true)
            if ($explicitProxyPassword -ne $null -and $explicitProxyPassword -ne '') {
                $passwd = ConvertTo-SecureString $explicitProxyPassword -AsPlainText -Force
                $proxy.Credentials = New-Object System.Management.Automation.PSCredential ($explicitProxyUser, $passwd)
            }

            Write-Debug "Using explicit proxy server '$explicitProxy'."
            $downloader.Proxy = $proxy

        }
        elseif (!$downloader.Proxy.IsBypassed($url)) {
            # system proxy (pass through)
            $creds = $defaultCreds
            if ($creds -eq $null) {
                Write-Debug "Default credentials were null. Attempting backup method"
                $cred = get-credential
                $creds = $cred.GetNetworkCredential();
            }

            $proxyaddress = $downloader.Proxy.GetProxy($url).Authority
            Write-Debug "Using system proxy server '$proxyaddress'."
            $proxy = New-Object System.Net.WebProxy($proxyaddress)
            $proxy.Credentials = $creds
            $downloader.Proxy = $proxy
        }
    }

    return $downloader
}


function Download-File {
    param (
        [string]$url,
        [string]$file
    )
    #Write-Output "Downloading $url to $file"
    $downloader = Get-Downloader $url
    
    $downloader.DownloadFile($url, $file)
}






# TMP dir & file
if ($env:TEMP -eq $null) {
    $env:TEMP = Join-Path $env:SystemDrive 'temp'
}
$chocTempDir = Join-Path $env:TEMP "choco-automation"
$tempDir = Join-Path $chocTempDir "choco-automation-install"
if (![System.IO.Directory]::Exists($tempDir)) { [void][System.IO.Directory]::CreateDirectory($tempDir) }
$file = Join-Path $tempDir "choco-automation.zip"

$chocInstallDir = "$Home\choco-automation\"


$url = 'https://gitlab.com/juguerre/choco-automation/-/archive/feature/remote-json-templates/choco-automation-feature-remote-json-templates.zip'

# Download the Chocolatey package
Write-Output "Getting Chocolatey from $url."
Download-File $url $file


# Determine unzipping method
# 7zip is the most compatible so use it by default
$7zaExe = Join-Path $tempDir '7za.exe'
$unzipMethod = '7zip'
$useWindowsCompression = $env:chocolateyUseWindowsCompression
if ($useWindowsCompression -ne $null -and $useWindowsCompression -eq 'true') {
    Write-Output 'Using built-in compression to unzip'
    $unzipMethod = 'builtin'
}
elseif (-Not (Test-Path ($7zaExe))) {
    Write-Output "Downloading 7-Zip commandline tool prior to extraction."
    # download 7zip
    Download-File 'https://chocolatey.org/7za.exe' "$7zaExe"
}


# unzip the package
mkdir $chocInstallDir
Write-Output "Extracting $file to $chocInstallDir..."
if ($unzipMethod -eq '7zip') {
    $params = "x -o`"$chocInstallDir`" -bd -y `"$file`""
    # use more robust Process as compared to Start-Process -Wait (which doesn't
    # wait for the process to finish in PowerShell v3)
    $process = New-Object System.Diagnostics.Process
    $process.StartInfo = New-Object System.Diagnostics.ProcessStartInfo($7zaExe, $params)
    $process.StartInfo.RedirectStandardOutput = $true
    $process.StartInfo.UseShellExecute = $false
    $process.StartInfo.WindowStyle = [System.Diagnostics.ProcessWindowStyle]::Hidden
    $process.Start() | Out-Null
    $process.BeginOutputReadLine()
    $process.WaitForExit()
    $exitCode = $process.ExitCode
    $process.Dispose()

    $errorMessage = "Unable to unzip package using 7zip. Perhaps try setting `$env:chocolateyUseWindowsCompression = 'true' and call install again. Error:"
    switch ($exitCode) {
        0 { break }
        1 { throw "$errorMessage Some files could not be extracted" }
        2 { throw "$errorMessage 7-Zip encountered a fatal error while extracting the files" }
        7 { throw "$errorMessage 7-Zip command line error" }
        8 { throw "$errorMessage 7-Zip out of memory" }
        255 { throw "$errorMessage Extraction cancelled by the user" }
        default { throw "$errorMessage 7-Zip signalled an unknown error (code $exitCode)" }
    }
}
else {
    if ($PSVersionTable.PSVersion.Major -lt 5) {
        try {
            $shellApplication = new-object -com shell.application
            $zipPackage = $shellApplication.NameSpace($file)
            $destinationFolder = $shellApplication.NameSpace($tempDir)
            $destinationFolder.CopyHere($zipPackage.Items(), 0x10)
        }
        catch {
            throw "Unable to unzip package using built-in compression. Set `$env:chocolateyUseWindowsCompression = 'false' and call install again to use 7zip to unzip. Error: `n $_"
        }
    }
    else {
        Expand-Archive -Path "$file" -DestinationPath "$tempDir" -Force
    }
}
$subdir = Get-ChildItem -Path $chocInstallDir -Directory -Force -ErrorAction SilentlyContinue | Select-Object FullName
Write-Host $subdir[0].FullName
$saveShorcutPS1 = Join-Path $subdir[0].FullName "create-shorcut.ps1"
& $saveShorcutPS1
